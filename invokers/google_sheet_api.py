from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from asogeneratorservices.utilities.sheets import getSheetTitles
from asogeneratorservices.utilities.rows import *

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Sheets API Python Quickstart'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
        'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else:  # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials


def main():

    info = loadInitialInfo()

    filteredRows = removeEmptyRows(info)

    SMCRows = getSMCRows(filteredRows)

    choices = extractSMCNamesFromRows(SMCRows)

    showAndGetSMCChoices(choices)

def getHttpInvoker():
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())

    return http


def getSheetsOfSpreadsheet(result):
    return result['sheets']


def getRangesOfSheetByName(sheetName):
    return sheetName + '!A1:M'


def loadInitialInfo():

    service = getAPIService()

    spreadsheetId = '1sZ8GRejgsgbjGKHyFfYixykiaRxJ4L7k_60oOi1xbzk'

    initialRange = 'A1:M'

    valueRange = service.spreadsheets().values().get(
        spreadsheetId=spreadsheetId, range=initialRange).execute()

    values = valueRange.get('values', [])

    return values


def getAPIService():

    http = getHttpInvoker()

    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')

    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)
    return service


def showAndGetSMCChoices(SMCs):

    pass



if __name__ == '__main__':
    main()
