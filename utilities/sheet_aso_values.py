logicalId = 'Logical ID'
version = 'Versi\xf3n'
SMCRegistryId =  'Registry ID (SMC)'
SNRegistryId = 'Registry ID (SN)'
URL = 'URL Base'
description = 'Descripci\xf3nn'
inputIterface = 'Interfaz Entrada'
outputInterface = 'Interfaz Salida'
SMCRowTitles = [u'', u'Logical ID', u'Versi\xf3n', u'Registry ID (SMC)', u'Registry ID (SN)',
         u'URL Base', u'Descripci\xf3n', u'Interfaz Entrada', u'Interfaz Salida']
SMCJSONKeys = {
    u'emptyCell' : '',
    u'Logical ID' : 'logicalId',
    u'Versi\xf3n' : 'version',
    u'Registry ID (SMC)' : 'registryIdSMC',
    u'Registry ID (SN)' : 'registryIdSN',
    u'URL Base' : 'URLBase',
    u'Descripci\xf3n' : 'description',
    u'Interfaz Entrada' : 'interfazEntrada',
    u'Interfaz Salida' : 'interfazSalida'
}
