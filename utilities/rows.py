from asogeneratorservices.utilities.sheet_aso_values import *
import json
import io


def removeEmptyRows(groupOfRows):
    filteredRows = filter(lambda x: x != [], groupOfRows)
    return filteredRows


def getSMCRows(filteredRows):
    SMCList = findSMCBlock(filteredRows)
    return SMCList


def findSMCBlock(groupRows):
    SMCList = []
    for index, row in enumerate(groupRows):
        isSMCRow = [item for item in row] == SMCRowTitles
        if isSMCRow:
            linesAfterTitles = 1
            for SMCIndex in range(index + linesAfterTitles, len(groupRows)):
                filteredRows = removeEmptyRows(groupRows[SMCIndex])
                SMCList.append(filteredRows)
            return SMCList
    return SMCList


def generateJSONListFromSMCRows(SMCs):
    JSONList = []
    for SMC in SMCs:
        SMCJSON = {}
        for index, SMCCell in enumerate(SMC):
            if SMCCell:
                SMCJSON[SMCJSONKeys[SMCRowTitles[index]]] = SMCCell
        JSONList.append(json.dumps(SMCJSON))

    return JSONList


def extractSMCNamesFromRows(SMCRow):
    SMCNames = []
    SMCNamePosition = 1
    for row in SMCRow:
        SMCNames.append(row[SMCNamePosition])

    return SMCNames


