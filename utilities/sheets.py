def getSheetTitles(sheets):
    sheetTitles = []
    for index, item in enumerate(sheets):
        title = item['properties']['title']
        sheetTitles.append(title)
    return sheetTitles
