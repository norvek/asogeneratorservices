import unittest
from asogeneratorservices.invokers.google_sheet_api import *
from asogeneratorservices.tests.variables import *
from mockito import *


class SheetInvokerTest(unittest.TestCase):

    def test_correct_ranges_of_sheet_by_name(self):
        for title in sheetTitles:
            expectedRange = title + '!A1:M'
            sheetRange = getRangesOfSheetByName(title)
            self.assertEquals(expectedRange, sheetRange)


    def test_correct_SMC_options(self):

        expectedList = [u'listSpeiAccounts', u'createSpeiAccount',
                        u'deleteSpeiAccount', u'modifySpeiAccount']

        showAndGetSMCChoices(expectedList)


if __name__ == '__main__':
    unittest.main()
