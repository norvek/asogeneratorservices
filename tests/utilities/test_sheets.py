import unittest
from asogeneratorservices.utilities import sheets
from asogeneratorservices.tests.variables import *

class TestSheetUtilities(unittest.TestCase):

    def test_get_sheet_titles(self):
        titles = sheets.getSheetTitles(sheetsCollection)
        self.assertEqual(cmp(sheetTitles, titles), 0)


if __name__ == '__main__':
    unittest.main()
