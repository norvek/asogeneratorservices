import unittest
from asogeneratorservices.utilities import rows
from asogeneratorservices.utilities.sheet_aso_values import *
from asogeneratorservices.tests.variables import *
import json

class TestRowsUtilities(unittest.TestCase):

    def test_return_not_empty_rows(self):
        validRows = 2
        rowsList =  [[], 'value_one', [] ,'value_two']
        expectedList = ['value_one','value_two']
        returnedRows = rows.removeEmptyRows(rowsList)
        self.assertEqual(validRows, len(returnedRows))
        self.assertTrue(set(expectedList).issuperset(returnedRows))

    def test_SMC_search(self):
        definedSMC = 4;
        SMCRows = rows.getSMCRows(rowsList)
        self.assertEqual(definedSMC, len(SMCs))
        self.assertEqual(SMCs, SMCRows)

    def test_generation_SMC_JSON(self):
        SMCJSONList = rows.generateJSONListFromSMCRows(SMCs)
        for SMCJSON in SMCJSONList:
            SMCObject = json.loads(SMCJSON)
            for SMC in SMCs:
                if str(SMCObject['logicalId']) in SMC:
                    for index, SMCCell in enumerate(SMC):
                        if SMCCell:
                           SMCObjectValue = SMCObject[SMCJSONKeys[SMCRowTitles[index]]]
                           self.assertEqual(SMCCell, SMCObjectValue)

    def test_extract_SMC_names_list(self):

        expectedList = [u'listSpeiAccounts', u'createSpeiAccount',
                        u'deleteSpeiAccount', u'modifySpeiAccount']

        SMCsNamesList = rows.extractSMCNamesFromRows(SMCs)

        self.assertTrue(SMCsNamesList)
        self.assertEqual(len(SMCs), len(SMCsNamesList))
        self.assertEqual(expectedList, SMCsNamesList)


if __name__ == '__main__':
    unittest.main()
